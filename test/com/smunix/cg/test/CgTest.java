package com.smunix.cg.test;

import static org.junit.Assert.*;

import java.io.PrintStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.smunix.cg.wlm.Central;
import com.smunix.wlm.annotation.WlmClass;
import com.smunix.wlm.annotation.WlmTag;
import com.smunix.wlm.spi.model.Workspace;

public class CgTest {

	static Logger logger;

	@XmlRootElement
	static abstract class Customer {
		@XmlAttribute(name = "name")
		abstract String getName();
	}

	static class CustomerImpl extends Customer {
		@Override
		String getName() {
			return "Customer Name";
		}

		public static CustomerImpl create() {
			return new CustomerImpl();
		}

	}

	static class WorkspaceImpl extends Workspace {
		@Override
		public String MultitreeWSId() {
			return Workspace.Attributes.MultitreeWSID;
		}

		public String getNonInheritedMethod() {
			return "getNonInheritedMethod";
		}

		public static Workspace create() {
			return new WorkspaceImpl();
		}

		@Override
		public void MultitreeWSId(String string) {
			// TODO Auto-generated method stub
			
		}
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		logger = Logger.getLogger(CgTest.class.getName());
		Central.manage(Workspace.class, Customer.class);
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMarshallingInheriedAnnotation() {
		Customer c = CustomerImpl.create();
		try {
			JAXBContext jc = JAXBContext.newInstance(Customer.class);
			Marshaller marshal = jc.createMarshaller();
			marshal.marshal(c, System.out);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testWlmAttribute() {
		Workspace w = WorkspaceImpl.create();
		printAllannotedMethods(w, System.out);
		printAllannotedMethods(new Workspace() {

			@Override
			public String MultitreeWSId() {
				return Workspace.Attributes.MultitreeWSID;
			}

			@Override
			public void MultitreeWSId(String string) {
				// TODO Auto-generated method stub
				
			}
		}, System.out);
		assertTrue("Finished", true);
		System.out.println("Finished");
	}

	@Test
	public void testCgWlmCreate() {
		List<Workspace> workspaces = new LinkedList<Workspace>();
		workspaces.add(Workspace.create ());
		workspaces.add(Workspace.test ());
		for (Workspace w : workspaces)
			w.MultitreeWSId("MultitreeWSId");
		for (Workspace w : workspaces)
			System.out.println (w.MultitreeWSId());
	}

	private void printAllannotedMethods(Workspace w, PrintStream out) {
		out.println("All WLM Annotations");
		printAllAnnotedMethods(WlmClass.class, w.getClass(), out);

	}

	private <T> void printAllAnnotedMethods(
			Class<? extends Annotation> annClazz, Class<T> clazz,
			PrintStream out) {
		logger.log(Level.INFO, "annotated class=" + annClazz + ", clazz="
				+ clazz);
		if (clazz != null && clazz.getAnnotation(annClazz) != null)
			printAllAnnotatedMethods(locateAnnotatedClass(clazz, annClazz), out);
	}

	private <T> Class<? super T> locateAnnotatedClass(Class<T> clazz,
			Class<? extends Annotation> annotationClazz) {
		logger.log(Level.INFO, "annotated class=" + annotationClazz
				+ ", clazz=" + clazz);
		for (Annotation an : clazz.getDeclaredAnnotations())
			if (an.annotationType().equals(annotationClazz))
				return clazz;
		return locateAnnotatedClass(clazz.getSuperclass(), annotationClazz);

	}

	/**
	 * @param clazz
	 * @param out
	 */
	private <T> void printAllAnnotatedMethods(Class<T> clazz, PrintStream out) {
		if (clazz == null)
			return;
		Set<Method> methods = new HashSet<Method>();
		peekWlmMethodsOnly(methods, clazz.getDeclaredMethods());
		for (Method f : methods) {
			out.println(f);
		}

	}

	private void peekWlmMethodsOnly(Set<Method> methods,
			Method[] declaredMethods) {
		for (Method m : declaredMethods) {
			for (Annotation ann : m.getAnnotations())
				if (ann.annotationType().isAnnotationPresent(WlmTag.class))
					methods.add(m);
		}

	}
}
