package com.smunix.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map.Entry;
import java.util.Properties;

import resources.Loader;

public class LoaderTest {
	public static void main(String[] args) {
		Loader l = new Loader() {			
			@Override
			protected InputStream doLoad(InputStream in) {
				return in;
			}
		};
		InputStream in = l.load ("testAllocationInstructionFIX44Sell.properties");
		Properties pr = new Properties ();
		try {
			pr.load(in);
			for (Entry<Object, Object> e : pr.entrySet()) {
				System.out.println(e);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
