package com.smunix.test.wlm.impl.model;

import com.smunix.wlm.annotation.WlmTest;
import com.smunix.wlm.spi.model.Workspace;

@WlmTest
public class WorkspaceImpl extends Workspace {
	String mtwsid;
	@Override
	public String MultitreeWSId() {
		return mtwsid;
	}

	@Override
	public void MultitreeWSId(String string) {
		mtwsid = string;
	}

}
