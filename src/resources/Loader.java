package resources;

import java.io.InputStream;

public abstract class Loader {
	public InputStream load(String filename) {
		return doLoad (Loader.class.getResourceAsStream(filename));
	}
	protected abstract InputStream doLoad (InputStream in);
}
