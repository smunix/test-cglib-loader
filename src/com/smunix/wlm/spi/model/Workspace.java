package com.smunix.wlm.spi.model;

import com.smunix.cg.wlm.Central;
import com.smunix.wlm.annotation.WlmAttribute;
import com.smunix.wlm.annotation.WlmClass;

@WlmClass (name = "DSXWLM_WS")
public abstract class Workspace {
	public static abstract class Attributes {
		public static final String MultitreeWSID = "MTWSID";
	}
	public static abstract class Select {

		public static final String Type = "DSXWLM_WS";
		public static final String SelectName = "name";
		public static final String SelectRevision = "revision";
	}
	
	public static Workspace create() {
		return Central.create(Workspace.class);
	}
	public static Workspace test() {
		return Central.test(Workspace.class);
	}
	public static Workspace get(String name) {
		Workspace w = Workspace.create();
//		w.Name (name);
		return w;
	}
	
	@WlmAttribute (name = Attributes.MultitreeWSID)
	abstract public String MultitreeWSId ();
	@WlmAttribute (name = Attributes.MultitreeWSID)
	abstract public void MultitreeWSId(String string);
}
