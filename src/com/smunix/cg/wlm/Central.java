package com.smunix.cg.wlm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.smunix.wlm.annotation.WlmClass;
import com.smunix.wlm.annotation.WlmTest;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

public abstract class Central {
	
	private abstract static class SpiProviderPredicate<T> {
		abstract boolean matches (Class<?> provider);
	}

	private abstract static class AbstractProviderManager {
		@SuppressWarnings("unchecked")
		public static <T> Class<? extends T> getProvider(Class<T> clazz, SpiProviderPredicate<T> p) {
			for (Class<?> provider : Cache.getInstance().getProviders (clazz)) {
				if (p.matches(provider))
					return (Class<? extends T>)provider;
			}
			return clazz;
		}
	}
	
	private abstract static class ProviderManager extends AbstractProviderManager {
		public static <T> Class<? extends T> getProvider(Class<T> clazz) {
			return getProvider (clazz, new SpiProviderPredicate<T>() {

				@Override
				boolean matches(Class<?> provider) {
					return !provider.isAnnotationPresent(WlmTest.class);
				}
			});
		}

	}
	
	
	private abstract static class TestProviderManager extends AbstractProviderManager {
		public static <T> Class<? extends T> getProvider(Class<T> clazz) {
			return getProvider (clazz, new SpiProviderPredicate<T>() {

				@Override
				boolean matches(Class<?> provider) {
					return provider.isAnnotationPresent(WlmTest.class);
				}
			});
		}

	}
	
	

	public abstract static class Cache {
		private static Cache CACHE = null;
		protected static final String SPI_PREFIX = "META-INF/services/";
		protected Set<Class<?> > classes = new HashSet<Class<?>>();
		protected Map<String, ? extends Method> methods = new HashMap<String, Method>();
		@SuppressWarnings("rawtypes")
		protected Map<Class, Class> spiMap = new HashMap<Class, Class> ();
		
		private static Cache create() {
			return new Cache() {
				class SpiElement {
					Class<?> spi;
					Enumeration<URL> urls;
					List<Class<?>> providers = new LinkedList<Class<?>>();
				}
				
				abstract class SpiVisitor {

					public abstract void visit(String clazzProvider);
					
				}
				
				Map<Class<?>, SpiElement> spiConfigurations = new HashMap<Class<?>, SpiElement> ();

				private Class<?> locate (Class<?> clazz, Class<? extends Annotation> annotationClazz) {
					for (Annotation ann : clazz.getDeclaredAnnotations())
						if (ann.annotationType().equals(annotationClazz))
							return clazz;
					return locate (clazz.getSuperclass(), annotationClazz);					
				}
				private void add(Class<?> clazz) {
					classes.add(clazz);
				}

				@Override
				public <T> void manage(Class<T> clazz) {
					if (clazz == null || !clazz.isAnnotationPresent(WlmClass.class))
						return;
					add (locate (clazz, WlmClass.class));					
				}
				@Override
				protected void initSPI() {
					ClassLoader cl = Thread.currentThread().getContextClassLoader();
					initSPIConfigurations (cl);					
				}
				private void initSPIConfigurations(final ClassLoader cl) {
					String cFullName;
					for (Class<?> c : classes) {
						if (spiConfigurations.containsKey(c))
							continue;
						cFullName = SPI_PREFIX + c.getName();
						final SpiElement spiElement = new SpiElement();
						{
							spiElement.spi = c;
							try {
								if (cl != null)
									spiElement.urls = cl.getResources(cFullName);
								else
									spiElement.urls = ClassLoader.getSystemResources(cFullName);
							} catch (IOException e) {
								e.printStackTrace();
								continue;
							}
							visitURLs (spiElement, new SpiVisitor() {

								@Override
								public void visit(String clazzProvider) {
									try {
										spiElement.providers.add(Class.forName(clazzProvider, true, cl));
									} catch (ClassNotFoundException e) {
										e.printStackTrace();
									}									
								}
							});
						}
						spiConfigurations.put(spiElement.spi, spiElement);			
					}
				}
				private void visitURLs(SpiElement spiElement,
						SpiVisitor spiVisitor) {
					while (spiElement.urls.hasMoreElements()) {
						URL url = spiElement.urls.nextElement();
						try {
							BufferedReader rin = new BufferedReader(new InputStreamReader(url.openStream(), "utf-8"));
							String line = null;
							while ((line = rin.readLine()) != null) {
								spiVisitor.visit (line);
							}							
						} catch (IOException e) {
							e.printStackTrace();
							continue;
						}
						spiElement.providers.add(null);
					}
					
				}
				@Override
				public List<Class<?>> getProviders(Class<?> clazz) {
					return spiConfigurations.get(clazz).providers;
				}

			};
		}

		public static Cache getInstance() {
			if (null == CACHE)
				CACHE = Cache.create();			
			return CACHE;
		}
		private void init() {
			initSPI ();			
		}

		protected abstract void initSPI();
		public abstract List<Class<?>> getProviders (Class<?> clazz);
		public abstract <T> void manage (Class<T> clazz);
	}

	public static <T> T create(final Class<T> clazz) {
		Enhancer e = new Enhancer();
		e.setSuperclass(ProviderManager.getProvider (clazz));
		e.setCallback(new MethodInterceptor() {
			@Override
			public Object intercept(Object obj, Method method, Object[] args,
					MethodProxy proxy) throws Throwable {
				if (Modifier.isAbstract(method.getModifiers())) {
					return "redirect me to central!";
				}
				return proxy.invokeSuper(obj, args);
			}
		});
		return clazz.cast(e.create ());
	}
	
	public static <T> T test(final Class<T> clazz) {
		Enhancer e = new Enhancer();
		e.setSuperclass(TestProviderManager.getProvider (clazz));
		e.setCallback(new MethodInterceptor() {
			@Override
			public Object intercept(Object obj, Method method, Object[] args,
					MethodProxy proxy) throws Throwable {
				return proxy.invokeSuper(obj, args);
			}
		});
		return clazz.cast(e.create ());
	}

	public static void manage (Class<?>...classes) {
		final Cache c = Cache.getInstance(); 
		for (Class<?> clazz : classes)
			c.manage(clazz);
		c.init();
	}
}
