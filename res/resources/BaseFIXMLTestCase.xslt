<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="text"/>

	<xsl:template match="*[not(@FIXTag)]">
		<xsl:apply-templates select="*" />
	</xsl:template>
 
	<xsl:template match="*[@FIXTag]">
			<xsl:value-of select="@FIXTag" />
			<xsl:text>=</xsl:text>
			<xsl:choose>
				<xsl:when test="@Value">
					<xsl:value-of select="@Value" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="text()" />
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>&#10;</xsl:text>
			<xsl:apply-templates select="*" />
	</xsl:template>

</xsl:stylesheet>